#!/usr/bin/env python
import sys

n_recall, n_gold, n_pred = 0, 0, 0

if len(sys.argv) == 1 or (len(sys.argv) == 2 and sys.argv[1] == '-v'):
    fp = sys.stdin
else:
    fp = open(sys.argv[1], 'r')

for data in fp.read().strip().split('\n\n'):
    for line in data.strip().split('\n'):
        tokens = line.split()
        score = 1
        if tokens[0] in ('i_mean', 'you_know'):
            score = 2
        if tokens[-1] != 'O':
            n_pred += score
        if tokens[-2] != 'O':
            n_gold += score
        if tokens[-1] != 'O' and tokens[-2] != 'O':
            n_recall += score

if n_recall == 0:
    f = 0.
else:
    p = float(n_recall) / n_pred
    r = float(n_recall) / n_gold
    f = p * r * 2 / (p + r) * 100
print "%.3f" % f

if sys.argv[-1] == '-v':
    print n_recall, n_gold, n_pred, p, r
