#include <iostream>
#include "cnn/cnn.h"
#include "cnn/expr.h"
#include "cnn/training.h"

int main(int argc, char* argv[]) {
  cnn::Initialize(argc, argv);

  cnn::Model m;
  cnn::Parameters* p_a = m.add_parameters({ 2 });


  cnn::SimpleSGDTrainer sgd(&m);
  for (unsigned iter = 0; iter < 10; ++iter) {
    float EL = 0.;
    for (unsigned j = 0; j < 10; ++j) {
      cnn::ComputationGraph cg;
      cnn::expr::Expression a = cnn::expr::parameter(cg, p_a);
      cnn::expr::Expression p = cnn::expr::pick(cnn::expr::softmax(a), (j == 8 ? 0U: 1U));
      float L = cnn::as_scalar(cg.forward());
      std::cerr << iter << " " << j << " " << L << std::endl;
      cg.backward();
      sgd.update(1.);
      EL += L;
    }
    std::cerr << "::" << iter << " " << EL << std::endl;
  }
  std::string dummy;
  std::cin >> dummy;
  return 0;
}