#ifndef __ENCDEC2_ENCDEC_SEQLABEL_TRANSDUCER_H__
#define __ENCDEC2_ENCDEC_SEQLABEL_TRANSDUCER_H__

#include <iostream>
#include <vector>

struct EncdecSeqlabelTransducer {
  unsigned B, I, E, S, O;
  EncdecSeqlabelTransducer(const std::vector<std::string>& id_to_edit_label);

  void transduce(std::vector<unsigned>& predict,
    unsigned begin, unsigned end, unsigned len);
};

#endif  //  end for __ENCDEC2_ENCDEC_SEQLABEL_TRANSDUCER_H__