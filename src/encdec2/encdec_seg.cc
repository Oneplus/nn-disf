#include "encdec_seg.h"


BiLSTMDeletionWithSegmentEncoderDecoder::BiLSTMDeletionWithSegmentEncoderDecoder(cnn::Model* m,
  unsigned size_w, unsigned dim_w,
  unsigned size_p, unsigned dim_p,
  unsigned size_pw, unsigned dim_pw,
  unsigned dim_i, unsigned n_layers, unsigned dim_h,
  const std::unordered_map<unsigned, std::vector<float>>& pretrain_emb,
  const std::vector<std::string>& id2fill_lab,
  const std::vector<std::string>& id2edit_lab,
  unsigned eos_, unsigned max_len, float dr, float ep) :
  EncdecSeqlabelTransducer(id2edit_lab),
  input_layer(m, size_w, dim_w, size_p, dim_p, size_pw, dim_pw, dim_i, pretrain_emb),
  encoder(m, n_layers, dim_i, dim_h),
  decoder(m, n_layers, dim_i, dim_h),
  seg_emb(*m, n_layers, dim_i, dim_h),
  scorer(m, dim_h, dim_h, dim_h, dim_h, dim_h, 1),
  pretrained(pretrain_emb), id_to_fill_label(id2fill_lab), id_to_edit_label(id2edit_lab),
  eos(eos_), max_seg_len(max_len), dropout_rate(dr), error_prob(ep) {

}

void BiLSTMDeletionWithSegmentEncoderDecoder::negative_loglikelihood(cnn::ComputationGraph* hg,
  const std::vector<unsigned>& raw_sentence,
  const std::vector<unsigned>& sentence,
  const std::vector<unsigned>& postag,
  const std::vector<unsigned>& correct_fill_labels,
  const std::vector<unsigned>& correct_edit_labels,
  double& n_correct) {
  auto len = sentence.size();
  std::vector<cnn::expr::Expression> exprs(len + 1);
  for (unsigned i = 0; i < len; ++i) {
    auto wid = sentence[i];
    auto pid = postag[i];
    auto pwid = raw_sentence[i];
    if (!pretrained.count(pwid)) { pwid = 0; }
    exprs[i] = input_layer.add_input(hg, wid, pid, pwid);
  }
  exprs[len] = input_layer.add_input(hg, eos, 0, eos);
  seg_emb.construct_chart(*hg, exprs, max_seg_len);

  encoder.set_dropout(dropout_rate);
  encoder.new_graph(hg);

  std::vector<BidirectionalLSTMLayer::Output> hidden1;
  encoder.add_inputs(hg, exprs);
  encoder.get_outputs(hg, hidden1);

  std::vector<cnn::expr::Expression> log_probs;
  std::vector<cnn::expr::Expression> dummy;

  decoder.set_dropout(dropout_rate);
  decoder.new_graph(hg);
  decoder.add_inputs(hg, dummy);

  unsigned i = 0;
  while (i <= len) {
    unsigned max_seg_end = i + max_seg_len;
    if (max_seg_end > len) { max_seg_end = len; }

    // calculate correct j.
    unsigned correct_j = i;
    if (correct_j < len) {
      while (correct_j <= max_seg_end) {
        if (correct_j == len || id_to_edit_label[correct_edit_labels[correct_j]][0] == 'O') {
          break;
        }
        correct_j++;
      }
    }
    BOOST_ASSERT_MSG(correct_j <= max_seg_end, "Correct not found in range.");

    std::vector<cnn::expr::Expression> a_scores;
    unsigned best_j = max_seg_end + 1; float best_score = 0.f;
    for (unsigned j = i; j <= max_seg_end; ++j) {
      auto& semb = seg_emb(i, j);
      cnn::expr::Expression s = scorer.get_output(hg,
        hidden1[j].first, hidden1[j].second, semb.first, semb.second, decoder.lstm.back());
      a_scores.push_back(s);

      float score = cnn::as_scalar(hg->get_value(s));
      if (best_j == max_seg_end + 1 || best_score < score) { best_j = j; best_score = score; }
    }

    log_probs.push_back(cnn::expr::pickneglogsoftmax(cnn::expr::concatenate(a_scores), correct_j - i));
    if (correct_j == best_j) {
      if (correct_j < len) { n_correct++; }
      if (correct_j < len && id_to_fill_label[correct_fill_labels[correct_j]][0] == 'O') {
        decoder.lstm.add_input(exprs[correct_j]);
      }
      i = correct_j + 1;
    } else {
      if (cnn::rand01() < error_prob) {
        if (best_j < len && id_to_fill_label[correct_fill_labels[best_j]][0] == 'O') {
          decoder.lstm.add_input(exprs[best_j]);
        }
        i = best_j + 1;
      } else {
        if (correct_j < len && id_to_fill_label[correct_fill_labels[correct_j]][0] == 'O') {
          decoder.lstm.add_input(exprs[correct_j]);
        }
        i = correct_j + 1;
      }
    }
  }
  cnn::expr::Expression tot_neglogprob = cnn::expr::sum(log_probs);
}

void BiLSTMDeletionWithSegmentEncoderDecoder::predict(cnn::ComputationGraph* hg,
  const std::vector<unsigned>& raw_sentence,
  const std::vector<unsigned>& sentence,
  const std::vector<std::string>& sentence_str,
  const std::vector<unsigned>& postag,
  const std::vector<unsigned>& correct_fill_labels,
  std::vector<unsigned>& predict_edit_labels) {
  auto len = sentence.size();
  std::vector<cnn::expr::Expression> exprs(len + 1);

  for (unsigned i = 0; i < len; ++i) {
    auto wid = sentence[i];
    auto pid = postag[i];
    auto pwid = raw_sentence[i];
    if (!pretrained.count(pwid)) { pwid = 0; }
    exprs[i] = input_layer.add_input(hg, wid, pid, pwid);
  }
  exprs[len] = input_layer.add_input(hg, eos, 0, eos);
  seg_emb.disable_dropout();
  seg_emb.construct_chart(*hg, exprs, max_seg_len);

  encoder.disable_dropout();
  encoder.new_graph(hg);
  encoder.add_inputs(hg, exprs);

  std::vector<BidirectionalLSTMLayer::Output> hidden1;
  encoder.get_outputs(hg, hidden1);

  std::vector<cnn::expr::Expression> dummy;
  decoder.disable_dropout();
  decoder.new_graph(hg);
  decoder.add_inputs(hg, dummy);

  unsigned i = 0;

  while (i <= len) {
    unsigned max_seg_end = i + max_seg_len;
    if (max_seg_end > len) { max_seg_end = len; }

    unsigned best_j = max_seg_end + 1; float best_score = 0.f;
    for (unsigned j = i; j <= max_seg_end; ++j) {
      auto& semb = seg_emb(i, j);
      cnn::expr::Expression s = scorer.get_output(hg,
        hidden1[j].first, hidden1[j].second, semb.first, semb.second, decoder.lstm.back());
      float score = cnn::as_scalar(hg->get_value(s));
      if (best_j == max_seg_end + 1 || best_score < score) { best_j = j; best_score = score; }
    }

    transduce(predict_edit_labels, i, best_j, len);
    if (best_j < len && id_to_fill_label[correct_fill_labels[best_j]][0] == 'O') {
      decoder.lstm.add_input(exprs[best_j]);
    }
    i = best_j + 1;
  }
}
