#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <algorithm>
#include <chrono>
#include <ctime>

#ifdef _MSC_VER
#include <io.h>
#else
#include <unistd.h>
#endif
#include <signal.h>

#include "utils.h"
#include "logging.h"
#include "training_utils.h"
#include "encdec2/c2.h"
#include "encdec2/models.h"
#include "cnn/training.h"
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>

namespace po = boost::program_options;

Corpus corpus = Corpus();

void init_command_line(int argc, char* argv[], po::variables_map* conf) {
  po::options_description opts("Configuration options");
  opts.add_options()
    ("graph", po::value<std::string>()->default_value("encdec_seg"), "The type of graph, avaliable [encdec_seg].")
    ("optimizer", po::value<std::string>()->default_value("simple_sgd"), "The optimizer.")
    ("eta", po::value<float>()->default_value(0.1), "the eta value.")
    ("training_data,T", po::value<std::string>(), "Training corpus.")
    ("dev_data,d", po::value<std::string>(), "Development corpus")
    ("pretrained,w", po::value<std::string>(), "Pretrained word embeddings")
    ("unk_strategy,o", po::value<unsigned>()->default_value(1), "Unknown word strategy: 1 = singletons become UNK with probability unk_prob")
    ("unk_prob,u", po::value<double>()->default_value(0.2), "Probably with which to replace singletons with UNK in training data")
    ("model,m", po::value<std::string>(), "Load saved model from this file")
    ("train,t", "Should training be run?")
    ("layers", po::value<unsigned>()->default_value(2), "number of LSTM layers")
    ("word_dim", po::value<unsigned>()->default_value(32), "input embedding size")
    ("pos_dim", po::value<unsigned>()->default_value(12), "POS dimension")
    ("pretrained_dim", po::value<unsigned>()->default_value(100), "pretrained input dimension")
    ("hidden_dim", po::value<unsigned>()->default_value(100), "hidden dimension")
    ("lstm_input_dim", po::value<unsigned>()->default_value(100), "LSTM input dimension")
    ("label_dim", po::value<unsigned>()->default_value(20), "label embedding size")
    ("maxiter", po::value<unsigned>()->default_value(10), "Max number of iterations.")
    ("max_seg_len", po::value<unsigned>()->default_value(10), "Max segment length.")
    ("dropout", po::value<float>()->default_value(0.), "The droput rate.")
    ("error_prob", po::value<float>()->default_value(0.2), "The error probability.")
    ("output", po::value<std::string>(), "the path to output file.")
    ("conlleval", po::value<std::string>()->default_value("./conlleval.sh"), "config path to the conlleval script")
    ("report_stops", po::value<unsigned>()->default_value(100), "the number of stops for reporting.")
    ("evaluate_stops", po::value<unsigned>()->default_value(2500), "the number of stops for evaluation.")
    ("verbose,v", "verbose log")
    ("help,h", "Help");

  po::options_description dcmdline_options;
  dcmdline_options.add(opts);
  po::store(parse_command_line(argc, argv, dcmdline_options), *conf);

  if (conf->count("help")) {
    std::cerr << dcmdline_options << std::endl;
    exit(1);
  }
  init_boost_log(conf->count("verbose"));
  if (conf->count("training_data") == 0) {
    _ERROR << "Please specify --training_data (-T): "
      << "this is required to determine the vocabulary mapping, even if the parser is used in prediction mode.";
    exit(1);
  }
}


std::string get_model_name(const po::variables_map& conf) {
  std::ostringstream os;
  os << "disf_encdec2.model." << conf["graph"].as<std::string>()
    << "_n" << conf["layers"].as<unsigned>()
    << "_w" << conf["word_dim"].as<unsigned>()
    << "_p" << conf["pos_dim"].as<unsigned>()
    << "_t" << conf["pretrained_dim"].as<unsigned>()
    << "_l" << conf["label_dim"].as<unsigned>()
    << "_i" << conf["lstm_input_dim"].as<unsigned>()
    << "_h" << conf["hidden_dim"].as<unsigned>()
    << "." << portable_getpid();
  return os.str();
}


double conlleval(const po::variables_map& conf,
  const std::string& tmp_output) {
#ifndef _MSC_VER
  std::string cmd = conf["conlleval"].as<std::string>() + " " + tmp_output;
  _TRACE << "Running: " << cmd << std::endl;
  FILE* pipe = popen(cmd.c_str(), "r");
  if (!pipe) {
    return 0.;
  }
  char buffer[128];
  std::string result = "";
  while (!feof(pipe)) {
    if (fgets(buffer, 128, pipe) != NULL) { result += buffer; }
  }
  pclose(pipe);

  std::stringstream S(result);
  std::string token;
  while (S >> token) {
    boost::algorithm::trim(token);
    return boost::lexical_cast<double>(token);
  }
#else
  return 1.;
#endif
  return 0.;
}

double evaluate(const po::variables_map& conf,
  SequenceLabelingModelWithFillGuidance& engine,
  const std::string& tmp_output,
  const std::set<unsigned>& training_vocab) {
  auto kUNK = corpus.get_or_add_word(Corpus::UNK);
  double n_total = 0;

  auto t_start = std::chrono::high_resolution_clock::now();

  std::ofstream ofs(tmp_output);
  for (unsigned sid = 0; sid < corpus.n_devel; ++sid) {
    const std::vector<unsigned>& raw_sentence = corpus.devel_sentences[sid];
    const std::vector<unsigned>& postag = corpus.devel_postags[sid];
    const std::vector<unsigned>& fill_label = corpus.devel_fill_labels[sid];
    const std::vector<unsigned>& edit_label = corpus.devel_edit_labels[sid];
    const std::vector<std::string>& sentence_str = corpus.devel_sentences_str[sid];

    unsigned len = raw_sentence.size();

    std::vector<unsigned> sentence = raw_sentence;
    for (auto& w : sentence) {
      if (training_vocab.count(w) == 0) w = kUNK;
    }

    BOOST_ASSERT_MSG(len == postag.size(), "Unequal sentence and postag length");
    BOOST_ASSERT_MSG(len == edit_label.size(), "Unequal sentence and gold label length");

    cnn::ComputationGraph hg;
    std::vector<unsigned> predict_label;
    engine.predict(&hg,
      raw_sentence, sentence, sentence_str, postag, fill_label, predict_label);

    BOOST_ASSERT_MSG(len == predict_label.size(), "Unequal sentence and predict label length");
    n_total += edit_label.size();
    for (unsigned i = 0; i < sentence.size(); ++i) {
      ofs << sentence_str[i] << " " <<
        corpus.id_to_postag[postag[i]] << " " <<
        corpus.id_to_edit_label[edit_label[i]] << " " <<
        corpus.id_to_edit_label[predict_label[i]] << std::endl;
    }
    ofs << std::endl;
  }
  ofs.close();
  auto t_end = std::chrono::high_resolution_clock::now();
  double f_score = conlleval(conf, tmp_output);
  _INFO << "TEST f-score: " << f_score <<
    " [" << corpus.n_devel <<
    " sents in " << std::chrono::duration<double, std::milli>(t_end - t_start).count() << " ms]";
  return f_score;
}


bool check_max_len(const std::vector<unsigned>& correct_labels, unsigned max_seg_len) {
  unsigned len = 0;
  for (unsigned i = 0; i < correct_labels.size(); ++i) {
    if (corpus.id_to_edit_label[correct_labels[i]][0] == 'O') {
      if (len > max_seg_len) { return false; }
      len = 0;
    } else {
      len += 1;
    }
  }
  if (len > max_seg_len) { return false; }
  return true;
}

void train(const po::variables_map& conf,
  cnn::Model& model,
  SequenceLabelingModelWithFillGuidance& engine,
  const std::string& model_name,
  const std::string& tmp_output,
  const std::set<unsigned>& vocabulary,
  const std::set<unsigned>& singletons) {
  _INFO << "start training ...";

  // Setup the trainer.
  cnn::Trainer* trainer = get_trainer(conf, &model);

  // Order for shuffle.
  std::vector<unsigned> order(corpus.n_train);
  for (unsigned i = 0; i < corpus.n_train; ++i) { order[i] = i; }

  unsigned kUNK = corpus.get_or_add_word(Corpus::UNK);
  auto maxiter = conf["maxiter"].as<unsigned>();
  double n_seen = 0;
  double n_corr_tokens = 0, n_tokens = 0, llh = 0;
  double batchly_n_corr_tokens = 0, batchly_n_tokens = 0, batchly_llh = 0;
  /* # correct tokens, # tokens, # sentence, loglikelihood.*/

  int logc = 0;
  _INFO << "number of training instances: " << corpus.n_train;
  _INFO << "going to train " << maxiter << " iterations.";

  auto unk_strategy = conf["unk_strategy"].as<unsigned>();
  auto unk_prob = conf["unk_prob"].as<double>();
  double best_f_score = 0.;

  for (unsigned iter = 0; iter < maxiter; ++iter) {
    _INFO << "start of iteration #" << iter << ", training data is shuffled.";
    // Use cnn::random generator to guarentee same result across different machine.
    std::shuffle(order.begin(), order.end(), (*cnn::rndeng));

    for (unsigned i = 0; i < order.size(); ++i) {
      auto sid = order[i];
      const std::vector<unsigned>& raw_sentence = corpus.train_sentences[sid];
      const std::vector<unsigned>& postag = corpus.train_postags[sid];
      const std::vector<unsigned>& fill_label = corpus.train_fill_labels[sid];
      const std::vector<unsigned>& edit_label = corpus.train_edit_labels[sid];

      if (!check_max_len(edit_label, 10)) {
        _WARN << "max length exceed at " << sid;
        continue;
      }

      std::vector<unsigned> sentence = raw_sentence;
      if (unk_strategy == 1) {
        for (auto& w : sentence) {
          if (singletons.count(w) && cnn::rand01() < unk_prob) { w = kUNK; }
        }
      }

      double lp;
      {
        cnn::ComputationGraph hg;
        engine.negative_loglikelihood(&hg, raw_sentence, sentence, postag, fill_label,
          edit_label, batchly_n_corr_tokens);

        lp = cnn::as_scalar(hg.forward());
        BOOST_ASSERT_MSG(lp >= 0, "Log prob < 0 on sentence");
        hg.backward();
        trainer->update(1.);
      }

      llh += lp; batchly_llh += lp;
      n_seen += 1;
      ++logc;
      n_tokens += edit_label.size(); batchly_n_tokens += edit_label.size();

      if (logc % conf["report_stops"].as<unsigned>() == 0) {
        trainer->status();
        _INFO << "iter (batch) #" << iter << " (epoch " << n_seen / corpus.n_train
          << ") llh: " << batchly_llh << " ppl: " << exp(batchly_llh / batchly_n_tokens)
          << " err: " << (batchly_n_tokens - batchly_n_corr_tokens) / batchly_n_tokens;
        n_corr_tokens += batchly_n_corr_tokens;
        batchly_llh = batchly_n_tokens = batchly_n_corr_tokens = 0.;
      }

      if (logc % conf["evaluate_stops"].as<unsigned>() == 0) {
        double f_score = evaluate(conf, engine, tmp_output, vocabulary);
        if (f_score > best_f_score) {
          best_f_score = f_score;
          _INFO << "new best record " << best_f_score << " is achieved, model updated.";
          std::ofstream out(model_name);
          boost::archive::text_oarchive oa(out);
          oa << model;
        }
      }
    }
    _INFO << "iteration #" << iter + 1 << " (epoch " << n_seen / corpus.n_train
      << ") llh: " << llh << " ppl: " << exp(llh / n_tokens)
      << " err: " << (n_tokens - n_corr_tokens) / n_tokens;
    llh = n_tokens = n_corr_tokens = 0.;

    double f_score = evaluate(conf, engine, tmp_output, vocabulary);
    if (f_score > best_f_score) {
      best_f_score = f_score;
      _INFO << "new best record " << best_f_score << " is achieved, model updated.";
      std::ofstream out(model_name);
      boost::archive::text_oarchive oa(out);
      oa << model;
    }
    if (conf["optimizer"].as<std::string>() == "simple_sgd" || conf["optimizer"].as<std::string>() == "momentum_sgd") {
      trainer->update_epoch();
    }
  }

  delete trainer;
}

int main(int argc, char* argv[]) {
  cnn::Initialize(argc, argv, 1234);
  std::cerr << "command:";
  for (unsigned i = 0; i < static_cast<unsigned>(argc); ++i) std::cerr << ' ' << argv[i];
  std::cerr << std::endl;

  po::variables_map conf;
  init_command_line(argc, argv, &conf);

  if (conf["unk_strategy"].as<unsigned>() == 1) {
    _INFO << "unknown word strategy: STOCHASTIC REPLACEMENT";
  } else {
    _INFO << "unknown word strategy: NO REPLACEMENT";
  }

  std::string model_name;
  if (conf.count("train")) {
    model_name = get_model_name(conf);
    _INFO << "going to write parameters to file: " << model_name;
  } else {
    model_name = conf["model"].as<std::string>();
    _INFO << "going to load parameters from file: " << model_name;
  }

  corpus.load_training_data(conf["training_data"].as<std::string>());
  unsigned eos = corpus.get_or_add_word("</s>");

  std::set<unsigned> training_vocab, singletons;
  corpus.get_vocabulary_and_singletons(training_vocab, singletons);

  std::unordered_map<unsigned, std::vector<float>> pretrained;
  if (conf.count("pretrained")) {
    load_pretrained_word_embedding(conf["pretrained"].as<std::string>(),
      conf["pretrained_dim"].as<unsigned>(), pretrained, corpus);
    _INFO << "pre-trained word embedding is loaded.";
  }

  cnn::Model model;
  SequenceLabelingModelWithFillGuidance* engine = nullptr;
  std::string graph = conf["graph"].as<std::string>();
  _INFO << "building " << graph;
  if (graph == "encdec_seg") {
    engine = new BiLSTMDeletionWithSegmentEncoderDecoder(&model,
      training_vocab.size() + 10, conf["word_dim"].as<unsigned>(),
      corpus.max_postag + 10, conf["pos_dim"].as<unsigned>(),
      corpus.max_word + 1, conf["pretrained_dim"].as<unsigned>(),
      conf["lstm_input_dim"].as<unsigned>(), conf["layers"].as<unsigned>(),
      conf["hidden_dim"].as<unsigned>(),
      pretrained, corpus.id_to_fill_label, corpus.id_to_edit_label,
      eos, conf["max_seg_len"].as<unsigned>(),
      conf["dropout"].as<float>(), conf["error_prob"].as<float>());
  } else {
    _ERROR << "Unknown graph type:" << conf["graph"].as<std::string>();
    exit(1);
  }

  corpus.load_devel_data(conf["dev_data"].as<std::string>());
  _INFO << "loaded " << corpus.n_devel << " devel sentences.";

  std::string tmp_output;
  if (conf.count("output")) {
    tmp_output = conf["output"].as<std::string>();
  } else {
    tmp_output = "disf_encdec.evaluator." + boost::lexical_cast<std::string>(portable_getpid());
#ifndef _MSC_VER
    tmp_output = "/tmp/" + tmp_output;
#endif
  }

  if (conf.count("train")) {
    train(conf, model, *engine, model_name, tmp_output, training_vocab, singletons);
  }

  std::ifstream in(model_name);
  boost::archive::text_iarchive ia(in);
  ia >> model;
  evaluate(conf, *engine, tmp_output, training_vocab);
  return 0;
}
