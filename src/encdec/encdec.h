#ifndef __ENCDEC_ENCDEC_H__
#define __ENCDEC_ENCDEC_H__

#include "layer.h"
#include "seq_labeling_model.h"
#include "encdec_seqlabel_transducer.h"

struct BiLSTMDeletionEncoderDecoder: public EncdecSeqlabelTransducer, public SequenceLabelingModel {
  StaticInputLayer input_layer;
  BidirectionalLSTMLayer bilstm_layer; // encoder layer
  LSTMLayer lstm_layer; //  decoder layer
  Merge3Layer merge_layer;
  const std::unordered_map<unsigned, std::vector<float>>& pretrained;
  const std::vector<std::string>& id_to_label;
  unsigned eos;
  unsigned max_seg_len;
  float dropout_rate;
  float error_prob;

  BiLSTMDeletionEncoderDecoder(cnn::Model* model,
    unsigned size_word,
    unsigned dim_word,
    unsigned size_postag,
    unsigned dim_postag,
    unsigned size_pretrained_word,
    unsigned dim_pretrained_word,
    unsigned dim_lstm_input,
    unsigned n_lstm_layers,
    unsigned dim_hidden,
    const std::unordered_map<unsigned, std::vector<float>>& pretrained_embedding,
    const std::vector<std::string>& id_to_label,
    unsigned eos, unsigned max_len,
    float dropout_rate,
    float error_prob = 0.2);

  void negative_loglikelihood(cnn::ComputationGraph* hg,
    const std::vector<unsigned>& raw_sentence,
    const std::vector<unsigned>& sentence,
    const std::vector<unsigned>& postags,
    const std::vector<unsigned>& correct_labels,
    double& n_corrects);

  void predict(cnn::ComputationGraph* hg,
    const std::vector<unsigned>& raw_sentence,
    const std::vector<unsigned>& sentence,
    const std::vector<std::string>& sentence_str,
    const std::vector<unsigned>& postags,
    std::vector<unsigned>& predict_labels);
};

#endif  //  end for __LABELER_LABELER_W_ATT_H__