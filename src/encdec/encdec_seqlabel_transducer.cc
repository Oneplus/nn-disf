#include "encdec_seqlabel_transducer.h"
#include "logging.h"

EncdecSeqlabelTransducer::EncdecSeqlabelTransducer(const std::vector<std::string>& id_to_label) {
  B = 0; I = 0; E = 0; S = 0; O = 0;
  for (unsigned i = 0; i < id_to_label.size(); ++i) {
    const std::string& token = id_to_label[i];
    if (token[0] == 'B') { B = i; }
    else if (token[0] == 'I') { I = i; }
    else if (token[0] == 'E') { E = i; }
    else if (token[0] == 'S') { S = i; }
    else if (token[0] == 'O') { O = i; }
    else { _ERROR << "label " << token << " not handled!";  }
  }
}

void EncdecSeqlabelTransducer::transduce(std::vector<unsigned>& predicted_labels,
  unsigned begin, unsigned end, unsigned len) {
  if (begin + 1 == end) {
    predicted_labels.push_back(S);
  } else {
    for (unsigned i = begin; i < end; ++i) {
      if (i == begin) { predicted_labels.push_back(B); }
      else if (i + 1 == end)  { predicted_labels.push_back(E); }
      else { predicted_labels.push_back(I); }
    }
  }
  if (end < len) {
    predicted_labels.push_back(O);
  }
}