#include "dec.h"
#include <boost/assert.hpp>


BiLSTMDeletionDecoder::BiLSTMDeletionDecoder(cnn::Model* model,
  unsigned size_word, unsigned dim_word,
  unsigned size_pos, unsigned dim_pos,
  unsigned size_pword, unsigned dim_pword,
  unsigned dim_lstm_input,
  unsigned n_lstm_layers,
  unsigned dim_hidden,
  const std::unordered_map<unsigned, std::vector<float>>& pretrained_embedding,
  const std::vector<std::string>& id2label,
  unsigned eos_, unsigned max_len,
  float dr, float ep):
  EncdecSeqlabelTransducer(id2label),
  input_layer(model, size_word, dim_word, size_pos, dim_pos, size_pword, dim_pword,
  dim_lstm_input, pretrained_embedding),
  lstm_layer(model, n_lstm_layers, dim_lstm_input, dim_hidden),
  merge_layer(model, dim_lstm_input, dim_hidden, 1),
  p_last(model->add_parameters({dim_lstm_input,})),
  pretrained(pretrained_embedding), id_to_label(id2label),
  eos(eos_), max_seg_len(max_len), dropout_rate(dr), error_prob(ep) {

}

void BiLSTMDeletionDecoder::negative_loglikelihood(cnn::ComputationGraph* hg,
  const std::vector<unsigned>& raw_sentence,
  const std::vector<unsigned>& sentence,
  const std::vector<unsigned>& postags,
  const std::vector<unsigned>& correct_labels,
  double& n_corrects) {

  auto len = sentence.size();
  std::vector<cnn::expr::Expression> exprs;

  exprs.resize(len + 1);
  for (unsigned i = 0; i < len; ++i) {
    auto wid = sentence[i];
    auto pid = postags[i];
    auto pre_wid = raw_sentence[i];
    if (!pretrained.count(pre_wid)) { pre_wid = 0; }
    exprs[i] = input_layer.add_input(hg, wid, pid, pre_wid);
  }
  exprs[len] = cnn::expr::parameter(*hg, p_last);

  std::vector<cnn::expr::Expression> log_probs;
  std::vector<cnn::expr::Expression> dummy;
  lstm_layer.new_graph(hg);
  lstm_layer.add_inputs(hg, dummy);

  unsigned i = 0;
  while (i <= len) {
    // calculate correct j. 
    unsigned max_seg_end = i + max_seg_len;
    if (max_seg_end > len) { max_seg_end = len; }

    unsigned correct_j = i;
    if (correct_j < len) {
      while (correct_j <= max_seg_end) {
        if (correct_j == len || id_to_label[correct_labels[correct_j]][0] == 'O') {
          break;
        }
        correct_j++;
      }
    }
    
    std::vector<cnn::expr::Expression> a_scores;
    unsigned best_j = len + 1; float best_score = 0.f;
    for (unsigned j = i; j <= max_seg_end; ++j) {
      cnn::expr::Expression s = merge_layer.get_output(hg, exprs[j], lstm_layer.lstm.back());
      float score = cnn::as_scalar(hg->get_value(s));
      if (best_j == len + 1 || best_score < score) { best_j = j; best_score = score; }
      a_scores.push_back(s);
    }

    log_probs.push_back(cnn::expr::pickneglogsoftmax(cnn::expr::concatenate(a_scores), correct_j - i));
    if (correct_j == best_j) {
      if (correct_j < len) { n_corrects++; }
      lstm_layer.lstm.add_input(exprs[correct_j]);
      i = correct_j + 1;
    } else {
      if (cnn::rand01() < error_prob) {
        lstm_layer.lstm.add_input(exprs[best_j]);
        i = best_j + 1;
      } else {
        lstm_layer.lstm.add_input(exprs[correct_j]);
        i = correct_j + 1;
      }
    }
  }
  cnn::expr::Expression tot_neglogprob = cnn::expr::sum(log_probs);
}

void BiLSTMDeletionDecoder::predict(cnn::ComputationGraph* hg,
  const std::vector<unsigned>& raw_sentence,
  const std::vector<unsigned>& sentence,
  const std::vector<std::string>& sentence_str,
  const std::vector<unsigned>& postags,
  std::vector<unsigned>& predict_labels) {

  auto len = sentence.size();
  std::vector<cnn::expr::Expression> exprs;

  exprs.resize(len + 1);
  for (unsigned i = 0; i < len; ++i) {
    auto wid = sentence[i];
    auto pid = postags[i];
    auto pre_wid = raw_sentence[i];
    if (!pretrained.count(pre_wid)) { pre_wid = 0; }
    exprs[i] = input_layer.add_input(hg, wid, pid, pre_wid);
  }
  exprs[len] = cnn::expr::parameter(*hg, p_last);

  std::vector<cnn::expr::Expression> dummy;
  lstm_layer.new_graph(hg);
  lstm_layer.add_inputs(hg, dummy);

  unsigned i = 0;
  while (i <= len) {
    unsigned max_seg_end = i + max_seg_len;
    if (max_seg_end > len) { max_seg_end = len; }

    unsigned best_j = len + 1; float best_score = 0.f;
    for (unsigned j = i; j <= max_seg_end; ++j) {
      cnn::expr::Expression s = merge_layer.get_output(hg, exprs[j], lstm_layer.lstm.back());
      float score = cnn::as_scalar(hg->get_value(s));
      if (best_j == len + 1 || best_score < score) { best_j = j; best_score = score; }
    }

    transduce(predict_labels, i, best_j, len);
    lstm_layer.lstm.add_input(exprs[best_j]);
    i = best_j + 1;
  }
}

