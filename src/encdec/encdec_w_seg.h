#ifndef __ENCDEC_ENCDEC_W_SEG_H__
#define __ENCDEC_ENCDEC_W_SEG_H__

#include "layer.h"
#include "seq_labeling_model.h"
#include "encdec_seqlabel_transducer.h"

struct BiLSTMDeletionWithSegmentEncoderDecoder : public EncdecSeqlabelTransducer, public SequenceLabelingModel {
  StaticInputLayer input_layer;
  BidirectionalLSTMLayer encoder;
  LSTMLayer decoder;
  SegBiEmbedding seg_emb;
  Merge5Layer scorer;
  const std::unordered_map<unsigned, std::vector<float>>& pretrained;
  const std::vector<std::string>& id_to_label;
  unsigned eos;
  unsigned max_seg_len;
  float dropout_rate;
  float error_prob;

  BiLSTMDeletionWithSegmentEncoderDecoder(cnn::Model* m,
    unsigned size_w, unsigned dim_w,
    unsigned size_p, unsigned dim_p,
    unsigned size_pt, unsigned dim_pt,
    unsigned dim_i, unsigned n_layers,
    unsigned dim_h,
    const std::unordered_map<unsigned, std::vector<float>>& pre_emb,
    const std::vector<std::string>& id2lab,
    unsigned eos, unsigned max_seg_len,
    float dr, float ep);

  void negative_loglikelihood(cnn::ComputationGraph* hg,
    const std::vector<unsigned>& raw_sentence,
    const std::vector<unsigned>& sentence,
    const std::vector<unsigned>& postag,
    const std::vector<unsigned>& correct_labels,
    double& n_corrects);

  void predict(cnn::ComputationGraph* hg,
    const std::vector<unsigned>& raw_sentence,
    const std::vector<unsigned>& sentence,
    const std::vector<std::string>& sentence_str,
    const std::vector<unsigned>& postag,
    std::vector<unsigned>& predict_labels);
};


#endif  //  end for __ENCDEC_ENCDEC_W_SEG_H__
