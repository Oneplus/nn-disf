#include "labeler_w_lm.h"
#include "logging.h"
#include <boost/assert.hpp>

BiLSTMDeletionLabelerWithLM::BiLSTMDeletionLabelerWithLM(cnn::Model* model,
  unsigned size_word, unsigned dim_word,
  unsigned size_postag, unsigned dim_postag,
  unsigned size_pretrained_word, unsigned dim_pretrained_word,
  unsigned dim_lstm_input, unsigned n_lstm_layers,
  unsigned dim_hidden, unsigned size_label,
  const std::unordered_map<unsigned, std::vector<float>>& pretrained_embedding,
  const std::vector<std::string>& id_to_label,
  float dr,
  bool enable_tran_constrain):
  LabelingDeletionModel(id_to_label, enable_tran_constrain),
  input_layer(model, size_word, dim_word, size_postag, dim_postag,
  size_pretrained_word, dim_pretrained_word, dim_lstm_input, pretrained_embedding),
  lstm_layer(model, n_lstm_layers, dim_lstm_input, dim_hidden),
  bilstm_layer(model, n_lstm_layers, dim_lstm_input, dim_hidden),
  merge3_layer(model, dim_hidden, dim_hidden, dim_hidden, dim_hidden),
  softmax_layer(model, dim_hidden, size_label),
  pretrained(pretrained_embedding),
  dropout_rate(dr), use_scheduled_sampling(false) {
}


void BiLSTMDeletionLabelerWithLM::negative_loglikelihood(cnn::ComputationGraph* hg,
  const std::vector<unsigned>& raw_sentence,
  const std::vector<unsigned>& sentence,
  const std::vector<unsigned>& postags,
  const std::vector<unsigned>& correct_labels,
  double &n_correct) {
  auto len = sentence.size();

  std::vector<cnn::expr::Expression> exprs;

  exprs.resize(len);
  for (unsigned i = 0; i < len; ++i) {
    auto wid = sentence[i];
    auto pid = postags[i];
    auto pre_wid = raw_sentence[i];
    if (!pretrained.count(pre_wid)) { pre_wid = 0; }
    exprs[i] = input_layer.add_input(hg, wid, pid, pre_wid);
  }

  lstm_layer.set_dropout(dropout_rate);
  bilstm_layer.set_dropout(dropout_rate);

  // The bi-lstm
  bilstm_layer.new_graph(hg);
  bilstm_layer.add_inputs(hg, exprs);

  std::vector<BidirectionalLSTMLayer::Output> hidden1;
  bilstm_layer.get_outputs(hg, hidden1);

  // The lstm / language model. Feed an empty input.
  std::vector<cnn::expr::Expression> dummy;
  lstm_layer.new_graph(hg);
  lstm_layer.add_inputs(hg, dummy);

  unsigned prev_lid = 0;
  std::vector<cnn::expr::Expression> log_probs;
  for (unsigned i = 0; i < len; ++i) {
    std::vector<unsigned> possible_labels;
    if (i == 0) {
      get_possible_labels(possible_labels);
    } else {
      get_possible_labels(prev_lid, possible_labels);
    }
    BOOST_ASSERT_MSG(possible_labels.size() > 0, "No possible labels, unexpected!");

    cnn::expr::Expression merged = merge3_layer.get_output(hg, 
      lstm_layer.lstm.back(), hidden1[i].first, hidden1[i].second);
    cnn::expr::Expression softmax_scores = softmax_layer.get_output(hg, 
      cnn::expr::dropout(merged, dropout_rate));
    
    std::vector<float> scores = cnn::as_vector(hg->get_value(softmax_scores));
    unsigned best_lid = get_best_scored_label(scores, possible_labels);
    unsigned lid = correct_labels[i];
    if (best_lid == correct_labels[i]) { ++n_correct; }
    log_probs.push_back(cnn::expr::pick(softmax_scores, lid));
    prev_lid = lid;

    // scheduled sampling.
    if (use_scheduled_sampling) {
      float flip_prob = ss_k / (ss_k + exp(ss_i / ss_k > 50. ? 50. : ss_i / ss_k));
      if (cnn::rand01() > flip_prob) {
        if (id_to_label[best_lid][0] == 'O') { lstm_layer.lstm.add_input(exprs[i]); }
      } else {
        if (id_to_label[lid][0] == 'O') { lstm_layer.lstm.add_input(exprs[i]); }
      }
      ss_i++;
    } else {
      if (id_to_label[lid][0] == 'O') { lstm_layer.lstm.add_input(exprs[i]); }
    }
  }

  cnn::expr::Expression tot_neglogprob = -cnn::expr::sum(log_probs);
}


void BiLSTMDeletionLabelerWithLM::predict(cnn::ComputationGraph* hg,
  const std::vector<unsigned>& raw_sentence,
  const std::vector<unsigned>& sentence,
  const std::vector<std::string>& sentence_str,
  const std::vector<unsigned>& postags,
  std::vector<unsigned>& predict_labels) {
  auto len = sentence.size();

  std::vector<cnn::expr::Expression> exprs;

  exprs.resize(len);
  for (unsigned i = 0; i < len; ++i) {
    auto wid = sentence[i];
    auto pid = postags[i];
    auto pre_wid = raw_sentence[i];
    if (!pretrained.count(pre_wid)) { pre_wid = 0; }
    exprs[i] = input_layer.add_input(hg, wid, pid, pre_wid);
  }

  lstm_layer.disable_dropout();
  bilstm_layer.disable_dropout();

  // The bi-lstm
  bilstm_layer.new_graph(hg);
  bilstm_layer.add_inputs(hg, exprs);

  std::vector<BidirectionalLSTMLayer::Output> hidden1;
  bilstm_layer.get_outputs(hg, hidden1);

  // The lstm / language model. Feed an empty input.
  std::vector<cnn::expr::Expression> dummy;
  lstm_layer.new_graph(hg);
  lstm_layer.add_inputs(hg, dummy);

  unsigned prev_lid = 0;
  for (unsigned i = 0; i < len; ++i) {
    std::vector<unsigned> possible_labels;
    if (i == 0) {
      get_possible_labels(possible_labels);
    } else {
      get_possible_labels(prev_lid, possible_labels);
    }
    BOOST_ASSERT_MSG(possible_labels.size() > 0, "No possible labels, unexpected!");

    cnn::expr::Expression merged = merge3_layer.get_output(hg, 
      lstm_layer.lstm.back(), hidden1[i].first, hidden1[i].second);
    cnn::expr::Expression softmax_scores = softmax_layer.get_output(hg, merged);
    
    std::vector<float> scores = cnn::as_vector(hg->get_value(softmax_scores));
    unsigned lid = get_best_scored_label(scores, possible_labels);
    prev_lid = lid;
    predict_labels.push_back(lid);

    if (id_to_label[lid][0] == 'O') {
      lstm_layer.lstm.add_input(exprs[i]);
    }
  }
}

void BiLSTMDeletionLabelerWithLM::set_scheduled_sampling(float k) {
  use_scheduled_sampling = true;
  ss_k = k;
  ss_i = 0.;
}