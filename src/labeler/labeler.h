#ifndef __LABELER_LABELER_H__
#define __LABELER_LABELER_H__

#include "layer.h"
#include "labeling_deletion_model.h"
#include "seq_labeling_model.h"


struct BiLSTMDeletionLabeler: public SequenceLabelingModel, public LabelingDeletionModel {
  StaticInputLayer input_layer;
  BidirectionalLSTMLayer bilstm_layer;
  Merge2Layer merge2_layer;
  SoftmaxLayer softmax_layer;
  const std::unordered_map<unsigned, std::vector<float>>& pretrained;
  float dropout_rate;

  BiLSTMDeletionLabeler(cnn::Model* model,
    unsigned size_word,
    unsigned dim_word,
    unsigned size_postag,
    unsigned dim_postag,
    unsigned size_pretrained_word,
    unsigned dim_pretrained_word,
    unsigned dim_lstm_input,
    unsigned n_lstm_layers,
    unsigned dim_hidden,
    unsigned size_label,
    const std::unordered_map<unsigned, std::vector<float>>& pretrained_embedding,
    const std::vector<std::string>& id_to_label,
    float dropout_rate,
    bool enable_tran_constrain);

  void negative_loglikelihood(cnn::ComputationGraph* hg,
    const std::vector<unsigned>& raw_sentence,
    const std::vector<unsigned>& sentence,
    const std::vector<unsigned>& postags,
    const std::vector<unsigned>& correct_labels,
    double& n_corrects);

  void predict(cnn::ComputationGraph* hg,
    const std::vector<unsigned>& raw_sentence,
    const std::vector<unsigned>& sentence,
    const std::vector<std::string>& sentence_str,
    const std::vector<unsigned>& postags,
    std::vector<unsigned>& predict_labels);
};


#endif  //  end for __LSTM_MODEL_H__