#include "labeler_w_att.h"

#include <boost/assert.hpp>

BiLSTMDeletionLabelerWithAttention::BiLSTMDeletionLabelerWithAttention(cnn::Model* model,
  unsigned size_word, unsigned dim_word,
  unsigned size_postag, unsigned dim_postag,
  unsigned size_pretrained_word, unsigned dim_pretrained_word,
  unsigned dim_lstm_input, unsigned n_lstm_layers,
  unsigned dim_hidden, unsigned size_label,
  const std::unordered_map<unsigned, std::vector<float>>& pretrained_embedding,
  const std::vector<std::string>& id_to_label,
  float dr,
  bool enable_tran_constrain):
  LabelingDeletionModel(id_to_label, enable_tran_constrain),
  input_layer(model, size_word, dim_word, size_postag, dim_postag,
  size_pretrained_word, dim_pretrained_word, dim_lstm_input, pretrained_embedding),
  bilstm_layer(model, n_lstm_layers, dim_lstm_input, dim_hidden),
  dist_embed(model, 4),
  a_func(model, dim_lstm_input, dim_lstm_input, 4, 1),
  merge3_layer(model, dim_hidden, dim_hidden, dim_hidden, dim_hidden),
  softmax_layer(model, dim_hidden, size_label),
  pretrained(pretrained_embedding),
  dropout_rate(dr) {
}


void BiLSTMDeletionLabelerWithAttention::negative_loglikelihood(cnn::ComputationGraph* hg,
  const std::vector<unsigned>& raw_sentence,
  const std::vector<unsigned>& sentence,
  const std::vector<unsigned>& postags,
  const std::vector<unsigned>& correct_labels,
  double& n_corrects) {
  auto len = sentence.size();

  std::vector<cnn::expr::Expression> exprs;

  exprs.resize(len);
  for (unsigned i = 0; i < len; ++i) {
    auto wid = sentence[i];
    auto pid = postags[i];
    auto pre_wid = raw_sentence[i];
    if (!pretrained.count(pre_wid)) { pre_wid = 0; }
    exprs[i] = input_layer.add_input(hg, wid, pid, pre_wid);
  }

  bilstm_layer.set_dropout(dropout_rate);
  bilstm_layer.new_graph(hg);
  bilstm_layer.add_inputs(hg, exprs);

  std::vector<BidirectionalLSTMLayer::Output> hidden1;
  bilstm_layer.get_outputs(hg, hidden1);

  unsigned prev_lid = id_to_label.size();
  std::vector<cnn::expr::Expression> log_probs;
  cnn::expr::Expression concated_inputs = cnn::expr::concatenate_cols(exprs);

  for (unsigned i = 0; i < len; ++i) {
    std::vector<unsigned> possible_labels;
    if (i == 0) {
      get_possible_labels(possible_labels);
    } else {
      get_possible_labels(prev_lid, possible_labels);
    }
    BOOST_ASSERT_MSG(possible_labels.size() > 0, "No possible labels, unexpected!");

    std::vector<cnn::expr::Expression> a_scores(len);
    for (unsigned j = 0; j < len; ++j) {
      a_scores[j] = a_func.get_output(hg, exprs[i], exprs[j],
        dist_embed.embed(hg, (int)i - j));
    }
    cnn::expr::Expression a = cnn::expr::softmax(cnn::expr::concatenate(a_scores));
    cnn::expr::Expression C = concated_inputs * a;
    cnn::expr::Expression merged1 = cnn::expr::rectify(merge3_layer.get_output(hg,
      hidden1[i].first, hidden1[i].second, C));
    cnn::expr::Expression softmax_scores = softmax_layer.get_output(hg,
      cnn::expr::dropout(merged1, dropout_rate));
    std::vector<float> scores = cnn::as_vector(hg->get_value(softmax_scores));

    unsigned best_lid = get_best_scored_label(scores, possible_labels);
    unsigned lid = correct_labels[i];
    if (best_lid == correct_labels[i]) { ++n_corrects; }
    log_probs.push_back(cnn::expr::pick(softmax_scores, lid));
    prev_lid = lid;
  }
  cnn::expr::Expression tot_neglogprob = -cnn::expr::sum(log_probs);
}


void BiLSTMDeletionLabelerWithAttention::predict(cnn::ComputationGraph* hg,
  const std::vector<unsigned>& raw_sentence,
  const std::vector<unsigned>& sentence,
  const std::vector<std::string>& sentence_str,
  const std::vector<unsigned>& postags,
  std::vector<unsigned>& predict_labels) {
  auto len = sentence.size();

  std::vector<cnn::expr::Expression> exprs;
  exprs.resize(len);
  for (unsigned i = 0; i < len; ++i) {
    auto wid = sentence[i];
    auto pid = postags[i];
    auto pre_wid = raw_sentence[i];
    if (!pretrained.count(pre_wid)) { pre_wid = 0; }
    exprs[i] = input_layer.add_input(hg, wid, pid, pre_wid);
  }

  bilstm_layer.disable_dropout();
  bilstm_layer.new_graph(hg);
  bilstm_layer.add_inputs(hg, exprs);

  std::vector<BidirectionalLSTMLayer::Output> hidden1;
  bilstm_layer.get_outputs(hg, hidden1);

  unsigned prev_lid = id_to_label.size();
  cnn::expr::Expression concated_inputs = cnn::expr::concatenate_cols(exprs);

 for (unsigned i = 0; i < len; ++i) {
    std::vector<unsigned> possible_labels;
    if (i == 0) {
      get_possible_labels(possible_labels);
    } else {
      get_possible_labels(prev_lid, possible_labels);
    }
    BOOST_ASSERT_MSG(possible_labels.size() > 0, "No possible labels, unexpected!");

    std::vector<cnn::expr::Expression> a_scores(len);
    for (unsigned j = 0; j < len; ++j) {
      a_scores[j] = a_func.get_output(hg, exprs[i], exprs[j],
        dist_embed.embed(hg, (int)i - j));
    }
    cnn::expr::Expression a = cnn::expr::softmax(cnn::expr::concatenate(a_scores));
    //cnn::expr::Expression C = cnn::expr::dot_product(a, concated_inputs);
    cnn::expr::Expression C = concated_inputs * a;
    cnn::expr::Expression merged1 = cnn::expr::rectify(merge3_layer.get_output(hg,
      hidden1[i].first, hidden1[i].second, C));

    cnn::expr::Expression softmax_scores = softmax_layer.get_output(hg, merged1);
    std::vector<float> scores = cnn::as_vector(hg->get_value(softmax_scores));

    unsigned best_lid = get_best_scored_label(scores, possible_labels);
    unsigned lid = best_lid;
    predict_labels.push_back(lid);
    prev_lid = lid;
  }
}
