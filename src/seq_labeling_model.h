#ifndef __LABELER_SEQ_LABELING_MODEL_H__
#define __LABELER_SEQ_LABELING_MODEL_H__

#include <vector>

struct SequenceLabelingModel {
  virtual void negative_loglikelihood(cnn::ComputationGraph* hg,
    const std::vector<unsigned>& raw_sentence,
    const std::vector<unsigned>& sentence,
    const std::vector<unsigned>& postags,
    const std::vector<unsigned>& correct_labels,
    double &n_correct) = 0;

  virtual void predict(cnn::ComputationGraph* hg,
    const std::vector<unsigned>& raw_sentence,
    const std::vector<unsigned>& sentence,
    const std::vector<std::string>& sentence_str,
    const std::vector<unsigned>& postags,
    std::vector<unsigned>& predict_labels) = 0;
};

struct SequenceLabelingModelWithFillGuidance {
  virtual void negative_loglikelihood(cnn::ComputationGraph* hg,
    const std::vector<unsigned>& raw_sentence,
    const std::vector<unsigned>& sentence,
    const std::vector<unsigned>& postags,
    const std::vector<unsigned>& correct_fill_labels,
    const std::vector<unsigned>& correct_edit_labels,
    double &n_correct) = 0;

  virtual void predict(cnn::ComputationGraph* hg,
    const std::vector<unsigned>& raw_sentence,
    const std::vector<unsigned>& sentence,
    const std::vector<std::string>& sentence_str,
    const std::vector<unsigned>& postags,
    const std::vector<unsigned>& correct_fill_labels,
    std::vector<unsigned>& predict_labels) = 0;
};


#endif  //  end for __LABELER_SEQ_LABELING_MODEL_H__